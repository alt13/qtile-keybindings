# Qtile keybindings

#### Python script that shows qtile keybindings

## How to install:
- For yad version install yad (display GTK+ dialogs in shell scripts)\
`sudo pacman -S yad`

- In `config.py` add keybindings\
Template:\
`#(GROUP) [Group name]` create group\
`Key([win], "c", lazy.window.kill(), desc="kill focused window"),` 	( use double quotes)\
`#(GROUP) [Group name 2]`\
`Key([win], "q", lazy.window.kill(), desc="kill focused window"),`

- Move  `.py` and `.sh` files to `~/.config/qtile/scripts/qtile_keybindings` 

- Create binding for `.sh` file

## Terminal version:
[Download](https://gitlab.com/alt13/qtile-keybindings/-/tree/main/1_terminal_version)

![](https://gitlab.com/alt13/qtile-keybindings/-/raw/main/screenshots/1.png)

## Yad version:
[Download](https://gitlab.com/alt13/qtile-keybindings/-/tree/main/2_yad_version)

![](https://gitlab.com/alt13/qtile-keybindings/-/raw/main/screenshots/2.png)

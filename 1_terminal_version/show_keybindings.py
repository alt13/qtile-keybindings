######################################
#     ___   _    _____ __   _____    #
#    / _ \ | |  |_   _/  | |____ |   #
#   / /_\ \| |    | | `| |     / /   #
#   |  _  || |    | |  | |     \ \   #
#   | | | || |____| | _| |_.___/ /   #
#   \_| |_/\_____/\_/ \___/\____/    #
#      https://gitlab.com/alt13      #
######################################
import re
import getpass
from colorama import Fore, Back, Style

CONFIG_PATH = '/home/' + getpass.getuser() + '/.config/qtile/config.py'


def read_conf_file():
    file = []

    # get shortcuts
    try:
        conf_file = open(CONFIG_PATH, 'r')
        for line in conf_file.readlines():
            if '#(GROUP)' in line:
                file.append(line)
            if 'Key(' in line:
                file.append(line)
        conf_file.close()
        return file
    except Exception as e:
        print(e)


def process_list(file):
    file = read_conf_file()
    group_name = ''
    key_bindings_list = []
    processed_dict = {}
    group_first = True

    for i in range(len(file)):

        if '#(GROUP)' in file[i]:
            # add keybindings to the group
            if group_first == False:
                # write and reset group list
                processed_dict[group_name] = key_bindings_list
                key_bindings_list = []
            group_first = False

            # get group name
            group_name = file[i]
            group_name = group_name[group_name.find('[')+1:group_name.find(']')]
            group_name = ' ' + group_name + ' '

        elif i == len(file)-1:
            # write and reset group list
            processed_dict[group_name] = key_bindings_list
            key_bindings_list = []
        else:
            keys = file[i]
            keys = keys.replace(' ', '')
            keys = keys.replace(keys[:keys.find('[')], '')
            keys = keys.replace(keys[keys.find(',lazy'):], '')
            keys = keys.replace('"', '')
            keys = keys.replace(',', ' + ')
            if 'desc' in file[i]:
                description = file[i]
                description = description.replace(description[:description.find('desc=')+5], '')
                temp = re.findall('"([^"]*)"', description)
                description = '- ' + temp[0]
            else:
                description = ''
            key_bindings_list.append([keys, description])
    return processed_dict


def show_shortcuts(dictionary):
    # not used: Back.RED
    colors_list = [Back.WHITE, Back.GREEN, Back.CYAN, Back.MAGENTA, Back.BLUE, Back.YELLOW]
    color_counter = 0
    space = False

    for group in dictionary:
        # add space
        if space == True:
            print()
        else:
            space = True

        # print group name
        if color_counter == len(colors_list):
            color_counter = 0
        print(Fore.BLACK + colors_list[color_counter] + Style.BRIGHT + group + Style.RESET_ALL)
        color_counter += 1

        # print 2 columns
        lens = []
        for col in zip(*dictionary[group]):
            lens.append(max([len(v) for v in col]))
        format = "  ".join(["{:<" + str(l) + "}" for l in lens])
        for row in dictionary[group]:
            print(format.format(*row))


# Start
show_shortcuts(process_list(read_conf_file()))

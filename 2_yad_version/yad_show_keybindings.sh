#!/usr/bin/env bash

# Set screen resolution
Xaxis=1920
Yaxis=1080

# Set frame size (percent of screen resolution)
Xpercent=75
Ypercent=75

let Xindex=$Xaxis*$Xpercent/100
let Yindex=$Yaxis*$Ypercent/100

python ~/.config/qtile/scripts/qtile_keybindings/show_keybindings.py | \
	yad \
	--text-info \
	--geometry=${Xindex}x${Yindex} \
	--window-icon='informazioni-symbolic' \
	--keep-icon-size \
	--title='Qtile keybindings' \
	--text='QTILE KEYBINDINGS' \
	--text-align=center \
	--no-buttons \
	


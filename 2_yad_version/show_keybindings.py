######################################
#     ___   _    _____ __   _____    #
#    / _ \ | |  |_   _/  | |____ |   #
#   / /_\ \| |    | | `| |     / /   #
#   |  _  || |    | |  | |     \ \   #
#   | | | || |____| | _| |_.___/ /   #
#   \_| |_/\_____/\_/ \___/\____/    #
#      https://gitlab.com/alt13      #
######################################
import re
import getpass

CONFIG_PATH = '/home/' + getpass.getuser() + '/.config/qtile/config.py'


def read_conf_file():
    file = []

    # get shortcuts
    try:
        conf_file = open(CONFIG_PATH, 'r')
        for line in conf_file.readlines():
            if '#(GROUP)' in line:
                file.append(line)
            if 'Key(' in line:
                file.append(line)
        conf_file.close()
        return file
    except Exception as e:
        print(e)


def process_list(file):
    file = read_conf_file()
    group_name = ''
    key_bindings_list = []
    processed_dict = {}
    group_first = True

    for i in range(len(file)):

        if '#(GROUP)' in file[i]:
            # add keybindings to the group
            if group_first == False:
                # write and reset group list
                processed_dict[group_name] = key_bindings_list
                key_bindings_list = []
            group_first = False

            # get group name
            group_name = file[i]
            group_name = group_name[group_name.find('['):group_name.find(']')+1]

        elif i == len(file)-1:
            # write and reset group list
            processed_dict[group_name] = key_bindings_list
            key_bindings_list = []
        else:
            keys = file[i]
            keys = keys.replace(' ', '')
            keys = keys.replace(keys[:keys.find('[')], '')
            keys = keys.replace(keys[keys.find(',lazy'):], '')
            keys = keys.replace('"', '')
            keys = keys.replace(',', ' + ')
            if 'desc' in file[i]:
                description = file[i]
                description = description.replace(description[:description.find('desc=')+5], '')
                temp = re.findall('"([^"]*)"', description)
                description = '- ' + temp[0]
            else:
                description = ''
            key_bindings_list.append([keys, description])
    return processed_dict


def show_shortcuts(dictionary):

    for group in dictionary:
        # print group name
        print(group)
        keybindings_list = dictionary[group]
        rows = len(dictionary[group])
        four_columns_list = []

        if (rows % 2) != 0:
            rows += 1
            keybindings_list.append(['', ''])

        half_rows = int(rows/2)
        for i in range(half_rows):
            four_columns_list.append([keybindings_list[i][0], keybindings_list[i][1]+'      ', keybindings_list[i+half_rows][0], keybindings_list[i+half_rows][1]])

        # print 4 columns
        lens = []
        for col in zip(*four_columns_list):
            lens.append(max([len(v) for v in col]))
        format = "  ".join(["{:<" + str(l) + "}" for l in lens])
        for row in four_columns_list:
            print(format.format(*row))

        print('\n')


# Start
print()
show_shortcuts(process_list(read_conf_file()))
